/*
 * @Author: YangZe
 * @Date: 2021-07-22 10:13:14
 * @Last Modified by: YangZe
 * @Last Modified time: 2021-07-22 10:37:27
 */
const path = require("path");
function addStyleResource(rule) {
  rule
    .use("style-resource")
    .loader("style-resources-loader")
    .options({
      patterns: [path.resolve(__dirname, "./src/assets/css/common.scss")],
    });
}

module.exports = {
  pwa: {
    iconPaths: {
      favicon32: "favicon.ico",
      favicon16: "favicon.ico",
      appleTouchIcon: "favicon.ico",
      maskIcon: "favicon.ico",
      msTileImage: "favicon.ico",
    },
  },
  lintOnSave: process.env.NODE_ENV === "development",
  devServer: {
    proxy: {
      "/dev-api": {
        // yapi请求
        // target: "https://yapi.142vip.cn/mock/393",
        // 服务器请求
        target: "http://111.203.59.61:8060/",
      },
    },
  },
  // // 向预处理器 Loader 传递选项
  chainWebpack: (config) => {
    const types = ["vue-modules", "vue", "normal-modules", "normal"];
    types.forEach((type) =>
      addStyleResource(config.module.rule("scss").oneOf(type))
    );
  },
};
