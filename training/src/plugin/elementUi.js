/*
 * @Author: YangZe
 * @Date: 2021-07-18 16:21:31
 * @Last Modified by: YangZe
 * @Last Modified time: 2021-07-29 08:11:03
 */
import {
  ElButton,
  ElInput,
  ElIcon,
  ElCheckbox,
  ElRadio,
  ElTabs,
  ElTabPane,
  ElDropdown,
  ElDropdownItem,
  ElDropdownMenu,
  ElMenu,
  ElMenuItem,
  ElSubmenu,
  ElTable,
  ElTableColumn,
  ElBreadcrumbItem,
  ElBreadcrumb,
  ElPagination,
  ElDialog,
  ElForm,
  ElFormItem,
  ElSelect,
  ElOption,
  ElDatePicker,
  ElTimePicker,
  ElRadioGroup,
  ElInputNumber,
  ElSwitch,
  ElMessage,
} from "element-plus";
// 引入面包屑组件
import Crumbs from "@/components/Crumbs/index.vue";
import Loading from "@/views/Loading";
import Table from "@/components/Table/index.vue";

export default {
  install(vue) {
    vue
      .use(ElButton)
      .use(ElInput)
      .use(ElIcon)
      .use(ElCheckbox)
      .use(ElRadio)
      .use(ElTabPane)
      .use(ElTabs)
      .use(ElDropdown)
      .use(ElDropdownItem)
      .use(ElDropdownMenu)
      .use(ElMenu)
      .use(ElMenuItem)
      .use(ElSubmenu)
      .use(ElTable)
      .use(ElTabs)
      .use(ElTable)
      .use(ElTableColumn)
      .use(ElBreadcrumbItem)
      .use(ElBreadcrumb)
      .use(ElPagination)
      .component("Crumbs", Crumbs) // 挂载全局组件 名字必须得一致
      .use(ElDialog)
      .use(ElForm)
      .use(ElFormItem)
      .use(ElSelect)
      .use(ElOption)
      .use(ElDatePicker)
      .use(ElTimePicker)
      .use(ElRadioGroup)
      .use(ElInputNumber)
      .component("Loading", Loading)
      .component("Table", Table)
      .use(ElSwitch)
      .use(ElDialog)
      .use(ElMessage);
  },
};
