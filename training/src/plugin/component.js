// 引入面包屑组件
import Crumbs from "@/components/Crumbs/index.vue";
import Loading from "@/views/Loading";
import Table from "@/components/Table/index.vue";

export default {
  install(vue) {
    vue
      .component("Crumbs", Crumbs) // 挂载全局组件 名字必须得一致
      .component("Loading", Loading)
      .component("Table", Table);
  },
};
