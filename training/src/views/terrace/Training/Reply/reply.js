export let column = [
  {
    label: "名称",
    slot: {
      body: "nameoption",
    },
  },
  {
    label: "专业",
    slot: {
      body: "majoroption",
    },
  },
  {
    label: "班级/计划",
    slot: {
      body: "classoption",
    },
  },
  {
    label: "发起人",
    slot: {
      body: "peooption",
    },
  },
  {
    label: "开始 / 截至时间",
    slot: {
      body: "timeoption",
    },
  },
  {
    label: "状态",
    slot: {
      body: "zhuangoption",
    },
  },
  {
    label: "操作",
    slot: {
      body: "zuooption",
    },
  },
];
export let statusArr = [
  {
    status: "0",
    msg: "全部",
  },
  {
    status: "1",
    msg: "未开始",
  },
  {
    status: "2",
    msg: "进行中",
  },
  {
    status: "3",
    msg: "已结束",
  },
];
