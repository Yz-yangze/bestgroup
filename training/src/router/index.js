/*
 * @Author: YangZe
 * @Date: 2021-07-18 15:47:02
 * @Last Modified by: YangZe
 * @Last Modified time: 2021-07-23 11:15:24
 * @Last Modified time: 2021-08-01 10:43:38
 */

import { createRouter, createWebHistory } from "vue-router";
import { whitList } from "./config";
import studentRouters from "./studentRouters";
import teacherRouters from "./teacherRoutes";
import NProgress from "nprogress";
import "nprogress/nprogress.css";

const routes = [
  // 一级页面
  {
    path: "/",
    redirect: "/login",
  },
  {
    path: "/login",
    name: "Login",
    component: () => import("../views/Login.vue"),
  },
  {
    path: "/mapbom",
    name: "MapBom",
    component: () => import("../views/terrace/MapBom/index.vue"),
  },
  {
    path: "/loading",
    name: "Loading",
    component: () => import("../views/Loading/index.vue"),
  },
  {
    path: "/index",
    name: "Index",
    component: () => import("../views/terrace/index.vue"),
    // 二级页面
    children: [
      // 二级重新定向
      {
        path: "/index",
        redirect: "/index/job",
      },
      // 岗位
      {
        path: "/index/job",
        name: "Job",
        component: () => import("../views/terrace/Job/Job.vue"),
      },
      {
        path: "/index/job/jobdel",
        name: "Jobdel",
        component: () => import("../views/terrace/Job/Jobdel.vue"),
      },
      // 项目
      {
        path: "/index/project",
        name: "Project",
        component: () => import("../views/terrace/Project/Project.vue"),
      },
      // 实训
      {
        path: "/index/training",
        name: "Training",
        component: () => import("../views/terrace/Training/Training.vue"),
      },
      //计划
      {
        path: "/index/training/trainingPlan",
        name: "TrainingPlan",
        component: () =>
          import("../views/terrace/Training/TrainingPlan/TrainingPlan.vue"),
      },
      // 添加计划
      {
        path: "/index/training/addPlan",
        name: "AddPlan",
        component: () => import("../views/terrace/Training/AddPlan/index.vue"),
      },
      //进度
      {
        path: "/index/training/schedule",
        name: "Schedule",
        component: () =>
          import("../views/terrace/Training/Schedule/Schedule.vue"),
      },
      //答辩
      {
        path: "/index/training/reply",
        name: "Reply",
        component: () => import("../views/terrace/Training/Reply/Reply.vue"),
      },
      // 添加答辩
      {
        path: "/index/training/defence",
        name: "Defence",
        component: () =>
          import("../views/terrace/Training/AddDefence/index.vue"),
      },
      //计划管理
      {
        path: "/index/training/planManagement",
        name: "PlanManagement",
        component: () =>
          import("../views/terrace/Training/PlanManagement/PlanManagement.vue"),
      },
      // 面试
      {
        path: "/index/interview/:interviewId",
        name: "Interview",
        component: () => import("../views/terrace/Interview/Interview.vue"),
      },
      //面试记录
      {
        path: "/index/interview/interviewRecord",
        name: "InterviewRecord",
        component: () =>
          import(
            "../views/terrace/Interview/InterviewRecord/InterviewRecord.vue"
          ),
      },
      //添加面试
      {
        path: "/index/interview/addinter",
        name: "Addinter",
        component: () =>
          import("../views/terrace/Interview/Addinter/Addinter.vue"),
      },
      //面试记录管理
      {
        path: "/index/interview/interviewLeader",
        name: "InterviewLeader",
        component: () =>
          import(
            "../views/terrace/Interview/InterviewLeader/InterviewLeader.vue"
          ),
      },
      //面试排行榜
      {
        path: "/index/interview/interviewRank",
        name: "InterviewRank",
        component: () =>
          import("../views/terrace/Interview/InterviewRank/InterviewRank.vue"),
      },
      // 学生端我的面试记录
      {
        path: "/index/interview/myInterviewLeader",
        name: "MyInterviewLeader",
        component: () =>
          import(
            "../views/terrace/Interview/MyInterviewLeader/MyInterviewLeader.vue"
          ),
      },
      // 问答
      {
        path: "/index/session",
        name: "Session",
        component: () => import("../views/terrace/Session/Session.vue"),
      },
      //问答列表
      {
        path: "/index/session/answerList",
        name: "AnswerList",
        component: () =>
          import("../views/terrace/Session/AnswerList/AnswerList.vue"),
      },
      //问答管理
      {
        path: "/index/session/answerLeader",
        name: "AnswerLeader",
        component: () =>
          import("../views/terrace/Session/AnswerLeader/AnswerLeader.vue"),
      },
      //问答列表详情页
      {
        path: "/index/session/answerDetail/:answerId",
        name: "AnswerDetail",
        component: () =>
          import("../views/terrace/Session/AnswerDetail/answerDetail.vue"),
      },
      // 我的问答  学生
      {
        path: "/index/session/myAnswerLeader",
        name: "MyAnswerLeader",
        component: () =>
          import("../views/terrace/Session/MyAnswerLeader/MyAnswerLeader.vue"),
      },
    ],
  },
];

const routerHistory = createWebHistory();
const router = createRouter({
  history: routerHistory,
  routes,
});
// 全局守卫
router.beforeEach((to, from, next) => {
  NProgress.start();
  // 先判断要去的路由是不是在白名单里，这些是大家都必须有的
  if (whitList.includes(to.name)) {
    next();
  } else if (!JSON.parse(localStorage.getItem("Authorization"))) {
    console.log(123123);
    next("/login");
  }
  // 还需要判断身份信息
  let { radio } = JSON.parse(localStorage.getItem("Authorization"));
  if (radio === "1") {
    // 是学生身份的话就判断学生数组里面有没有这个路由，没有的话就重新定向到登录页面
    // 利用findeIndex判断是否存在
    if (studentRouters.findIndex((item) => item.name === to.name) > -1) {
      next();
    } else {
      next("/login");
    }
  } else {
    // 老师身份的话就判断学生数组里面有没有这个路由，没有的话就重新定向到登录页面
    // 利用findeIndex判断是否存在
    if (teacherRouters.findIndex((item) => item.name === to.name) > -1) {
      next();
    } else {
      next("/login");
    }
  }
});
// 进度条
router.afterEach(() => {
  NProgress.done();
});

export default router;
