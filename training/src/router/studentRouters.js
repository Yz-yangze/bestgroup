const studentRouters = [
  // 岗位
  {
    path: "/index/job",
    name: "Job",
    component: () => import("../views/terrace/Job/Job.vue"),
  },
  // 项目
  {
    path: "/index/project",
    name: "Project",
    component: () => import("../views/terrace/Project/Project.vue"),
  },
  // 实训
  {
    path: "/index/training",
    name: "Training",
    component: () => import("../views/terrace/Training/Training.vue"),
  },
  // 面试
  {
    path: "/index/interview/:interviewId",
    name: "Interview",
    component: () => import("../views/terrace/Interview/Interview.vue"),
  },
  //面试记录
  {
    path: "/index/interview/interviewRecord",
    name: "InterviewRecord",
    component: () =>
      import("../views/terrace/Interview/InterviewRecord/InterviewRecord.vue"),
  },
  // 添加面试
  {
    path: "/index/interview/addinter",
    name: "Addinter",
    component: () => import("../views/terrace/Interview/Addinter/Addinter.vue"),
  },
  //我的面试记录
  {
    path: "/index/interview/myInterviewLeader",
    name: "MyInterviewLeader",
    component: () =>
      import(
        "../views/terrace/Interview/MyInterviewLeader/MyInterviewLeader.vue"
      ),
  },
  //面试排行榜
  {
    path: "/index/interview/interviewRank",
    name: "InterviewRank",
    component: () =>
      import("../views/terrace/Interview/InterviewRank/InterviewRank.vue"),
  },
  // 问答
  {
    path: "/index/session",
    name: "Session",
    component: () => import("../views/terrace/Session/Session.vue"),
  },
  //问答列表
  {
    path: "/index/session/answerList",
    name: "AnswerList",
    component: () =>
      import("../views/terrace/Session/AnswerList/AnswerList.vue"),
  },
  //问答列表详情页
  {
    path: "/index/session/answerDetail/:answerId",
    name: "AnswerDetail",
    component: () =>
      import("../views/terrace/Session/AnswerDetail/answerDetail.vue"),
  },
  //问答管理
  {
    path: "/index/session/answerLeader",
    name: "AnswerLeader",
    component: () =>
      import("../views/terrace/Session/AnswerLeader/AnswerLeader.vue"),
  },
  // 我的问答
  {
    path: "/index/session/myAnswerLeader",
    name: "MyAnswerLeader",
    component: () =>
      import("../views/terrace/Session/MyAnswerLeader/MyAnswerLeader.vue"),
  },
];
export default studentRouters;
