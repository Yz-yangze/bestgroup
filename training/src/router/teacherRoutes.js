const teacherRoutes = [
  // 岗位
  {
    path: "/index/job",
    name: "Job",
    component: () => import("../views/terrace/Job/Job.vue"),
  },
  //地图
  {
    path: "/index/map",
    name: "Map",
    component: () => import("../views/terrace/Map.vue"),
  },
  {
    path: "/index/job/jobdel",
    name: "Jobdel",
    component: () => import("../views/terrace/Job/Jobdel.vue"),
  },
  // 项目
  {
    path: "/index/project",
    name: "Project",
    component: () => import("../views/terrace/Project/Project.vue"),
  },
  // 实训
  {
    path: "/index/training",
    name: "Training",
    component: () => import("../views/terrace/Training/Training.vue"),
  },
  //计划
  {
    path: "/index/training/trainingPlan",
    name: "TrainingPlan",
    component: () =>
      import("../views/terrace/Training/TrainingPlan/TrainingPlan.vue"),
  },
  // 添加计划
  {
    path: "/index/training/addPlan",
    name: "AddPlan",
    component: () => import("../views/terrace/Training/AddPlan/index.vue"),
  },
  //进度
  {
    path: "/index/training/schedule",
    name: "Schedule",
    component: () => import("../views/terrace/Training/Schedule/Schedule.vue"),
  },
  //答辩
  {
    path: "/index/training/reply",
    name: "Reply",
    component: () => import("../views/terrace/Training/Reply/Reply.vue"),
  },
  // 添加答辩
  // 添加答辩
  {
    path: "/index/training/defence",
    name: "Defence",
    component: () => import("../views/terrace/Training/AddDefence/index.vue"),
  },
  //计划管理
  {
    path: "/index/training/planManagement",
    name: "PlanManagement",
    component: () =>
      import("../views/terrace/Training/PlanManagement/PlanManagement.vue"),
  },
  // 面试
  {
    path: "/index/interview/:interviewId",
    name: "Interview",
    component: () => import("../views/terrace/Interview/Interview.vue"),
  },
  //面试记录
  {
    path: "/index/interview/interviewRecord",
    name: "InterviewRecord",
    component: () =>
      import("../views/terrace/Interview/InterviewRecord/InterviewRecord.vue"),
  },
  //面试记录管理
  {
    path: "/index/interview/interviewLeader",
    name: "InterviewLeader",
    component: () =>
      import("../views/terrace/Interview/InterviewLeader/InterviewLeader.vue"),
  },
  //面试排行榜
  {
    path: "/index/interview/interviewRank",
    name: "InterviewRank",
    component: () =>
      import("../views/terrace/Interview/InterviewRank/InterviewRank.vue"),
  },
  // 问答
  {
    path: "/index/session",
    name: "Session",
    component: () => import("../views/terrace/Session/Session.vue"),
  },
  //问答列表
  {
    path: "/index/session/answerList/answerId",
    name: "AnswerList",
    component: () =>
      import("../views/terrace/Session/AnswerList/AnswerList.vue"),
  },
  //问答列表详情页
  {
    path: "/index/session/answerDetail/:answerId",
    name: "AnswerDetail",
    component: () =>
      import("../views/terrace/Session/AnswerDetail/answerDetail.vue"),
  },
  //问答管理
  {
    path: "/index/session/answerLeader",
    name: "AnswerLeader",
    component: () =>
      import("../views/terrace/Session/AnswerLeader/AnswerLeader.vue"),
  },
];

export default teacherRoutes;
