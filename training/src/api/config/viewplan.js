/*
 * @Author: YangZe
 * @Date: 2021-07-29 15:40:10
 * @Last Modified by: YangZe
 * @Last Modified time: 2021-07-30 15:05:36
 */

export const selectClassPlan = {
  url: "/dev-api/sxpt/progress/selectClassPlan",
  method: "get",
};

export const selectClassPlanInit = {
  url: "/dev-api/sxpt/progress/selectClassPlanInit",
  method: "get",
};

// 获取排行耪
export const classRank = {
  url: "/dev-api/sxpt/progress/classRank",
  method: "get",
};

// 动态传参过来以后发送的请求
export const selectClassPlanInitAll = {
  url: "/dev-api/sxpt/progress/selectClassPlanInitAll",
  method: "get",
};
