export const _getMajorList = {
  url: "/dev-api/sxpt/station/selectStationLabel",
  method: "get",
};
export const _getPostList = {
  url: "/dev-api/sxpt/station/selectStationVersionList",
  method: "get",
};
export const _deletePost = {
  url: "/dev-api/sxpt/station/deleteStation",
  method: "delete",
};
export const _updateStatus = {
  url: "/dev-api/sxpt/station/updateStationStatus",
  method: "get",
};
export const _updateCance = {
  url: "/dev-api/sxpt/station/updateStationStatusCancel",
  method: "get",
};
