/*
 * @Author: YangZe
 * @Date: 2021-07-18 19:25:45
 * @Last Modified by: YangZe
 * @Last Modified time: 2021-07-18 19:27:15
 */

export const getRouters = {
  url: "/dev-api/getRouters",
  method: "get",
};
