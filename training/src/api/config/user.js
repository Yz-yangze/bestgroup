/*
 * @Author: YangZe
 * @Date: 2021-07-18 14:58:10
 * @Last Modified by: YangZe
 * @Last Modified time: 2021-07-19 15:52:07
 */

// 获取验证码接口
export const getCaptcha = {
  url: "/dev-api/captchaImage",
  method: "get",
};

// 登录接口
export const getLogin = {
  url: "/dev-api/login",
  method: "post",
};
