/*
 * @Author: YangZe
 * @Date: 2021-07-21 16:35:31
 * @Last Modified by: YangZe
 * @Last Modified time: 2021-07-30 08:25:14
 */

export const getClassInfo = {
  url: "/dev-api/sxpt/classPlan/getClassInfo",
  method: "get",
};

export const getPlanList = {
  url: "/dev-api/sxpt/classPlan/getPlanList",
  method: "get",
};

// 计划管理页面
export const getPlanListAll = {
  url: "/dev-api/sxpt/classPlan/getPlanListAll",
  method: "get",
};
