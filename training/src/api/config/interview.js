import axios from "../../untils/httpAxios";
// 获取专业接口
export const getStationLabel = {
  url: "/dev-api/sxpt/station/selectStationLabel",
  method: "get",
};
// 获取表格接口
export const getinterviewList = {
  url: "/dev-api/sypt/interview/interviewList",
  method: "get",
};
// 获取面试记录榜单
export const getinterviewmianshi = {
  url: "/dev-api/sypt/interview/interviewRecordRangking",
  method: "get",
};
// 获取面试题榜单
export const getinterviewmianshiti = {
  url: "/dev-api/sypt/interview/interviewAnswerRangking",
  method: "get",
};
// 添加
export const getinterview = {
  url: "/dev-api/sypt/interview",
  method: "post",
};
// 详情
export const getdetail = (interviewId) => {
  return axios.get(`/dev-api/sypt/interview/info/${interviewId}`);
};
// 删除
export const getdelete = (interviewId) => {
  return axios.delete(
    `/dev-api/sypt/interview/deleteInterviewById/${interviewId}`
  );
};
