/*
 * @Author: YangZe
 * @Date: 2021-07-26 17:49:31
 * @Last Modified by: YangZe
 * @Last Modified time: 2021-07-29 07:43:45
 */

export const selectStationLabel = {
  url: "/dev-api/sxpt/station/selectStationLabel",
  method: "get",
};

export const getDefenceList = {
  url: "/dev-api/sxpt/defence/getDefenceList",
  method: "get",
};

export const getClassInfo = {
  url: "/dev-api/sxpt/classPlan/getClassInfo",
  method: "get",
};

export const getClassStudent = {
  url: "/dev-api/sxpt/classPlan/getClassStudent/:id",
  method: "get",
};
