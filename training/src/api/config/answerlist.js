import axios from "@/untils/httpAxios";
//获取问答列表数据
export const answer = {
  url: "/dev-api/sypt/answer/list",
  method: "get",
};
//添加数据
export const Answers = {
  url: "/dev-api/sypt/answer",
  method: "post",
};
//跳转详情页
export const answerDetail = (answerId) => {
  return axios.get(`/dev-api/sypt/answer/${answerId}`);
};

//问答管理  所有数据
export const anserman = {
  url: "/dev-api/sxpt/answer/wait",
  method: "get",
};

//学生端  我的问题
export const MyAnswer = {
  url: "/dev-api/sypt/answer/myAsk",
  method: "get",
};

//学生端  我的答案
export const MyAsk = {
  url: "/dev-api/sypt/answer/myAnswer",
  method: "get",
};

//学生端 问答条数
export const AnswerReply = {
  url: "/dev-api/sypt/answer/reply",
  method: "get",
};
// /dev-api/sypt/reply/answerList
export const AllQuestions = {
  url: "/dev-api/sypt/reply/answerList",
  method: "get",
};
