/*
 * @Author: YangZe
 * @Date: 2021-07-31 11:28:42
 * @Last Modified by: YangZe
 * @Last Modified time: 2021-07-31 12:29:34
 */

export const selectStationLabel = {
  url: "/dev-api/sxpt/station/selectStationLabel",
  method: "get",
};
