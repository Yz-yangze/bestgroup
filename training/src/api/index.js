/*
 * @Author: YangZe
 * @Date: 2021-07-18 14:57:07
 * @Last Modified by: YangZe
 * @Last Modified time: 2021-07-29 07:44:52
 */

import axios from "../untils/httpAxios";
// import axios from "axios";

// webpack有一个require方法，官网可查
//（创建出）一个 context，其中文件来自 test 目录，request 以 `.js` 结尾。
const dirdata = require.context("./config", true, /\.js$/); // 有一个keys方法，本身也是一个方法，可以获取到模块对象
const res = dirdata.keys().reduce((val, ite) => {
  let apis = dirdata(ite); // 获得的是模块对象，里面有文件夹里抛出的对象
  // 命名空间
  const [, namespace] = ite.match(/\/(\w+)\.js$/); // 字符串有一个match方法，里面写正则
  val[namespace] = Object.keys(apis).reduce((val1, item) => {
    val1[item] = (data = {}) => {
      // 判断是get请求还是post请求
      if (apis[item].method === "post") {
        return axios({ ...apis[item], data });
      } else {
        return axios({
          url: apis[item].url.replace(/:(\w+)/g, ($s1, $s2) => {
            return data[$s2];
          }),
          method: apis[item].method,
          params: data,
        });
      }
    };
    return val1;
  }, {});
  return val;
}, {});
export default res;
