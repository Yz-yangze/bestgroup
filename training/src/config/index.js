export const statusList = [
  { id: "1", name: "草稿" },
  { id: "2", name: "已发布" },
  { id: "3", name: "待审核" },
  { id: "4", name: "已驳回" },
];
