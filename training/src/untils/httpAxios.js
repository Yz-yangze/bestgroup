/*
 * @Author: YangZe
 * @Date: 2021-07-18 19:32:10
 * @Last Modified by: YangZe
 * @Last Modified time: 2021-07-20 20:35:18
 */
import axios from "axios";
import { ElMessage } from "element-plus";
import code from "./httpCode";

const httpAxios = axios.create({
  url: "http://111.203.59.61:8060/",
  timeout: 500000, // 时间，过了这个时间就会报错
});

// 添加请求拦截器
httpAxios.interceptors.request.use(
  function (config) {
    // 在发送请求添加身份验证信息，就是token
    return {
      ...config,
      headers: {
        ...config.headers,
        Authorization: localStorage.getItem("Authorization")
          ? JSON.parse(localStorage.getItem("Authorization"))?.token
          : "",
      },
    };
  },
  function (error) {
    // 对请求错误做些什么  一般不会进到这个方法1
    return Promise.reject(error);
  }
);

// 添加响应拦截器
httpAxios.interceptors.response.use(
  function (response) {
    // 对响应数据做点什么
    return response;
  },
  function (error) {
    // 对响应错误做点什么
    switch (error.code) {
      case "ECONNABORTED":
        // 超过httpAxios定义的时间就会报这个错
        ElMessage.error("请求超时");
        break;

      default:
        break;
    }
    // 一些常见的错误码
    code[error.code] && ElMessage.error(code[error.code]);
    return Promise.reject(error);
  }
);

export default httpAxios;
